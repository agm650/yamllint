'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import * as cp from 'child_process';
import * as rl from 'readline';

const MODE = {
  language: 'yaml',
  scheme: 'file'
};

function SEVERITY(level: String) {
  if (level === 'warning') {
    return vscode.DiagnosticSeverity.Warning;
  } else if(level === 'error') {
    return vscode.DiagnosticSeverity.Error;
  } else {
    return vscode.DiagnosticSeverity.Error;
  }
}

let diagnostics: vscode.DiagnosticCollection;

function checkSanity() {
  let yamllint_exe = vscode.workspace.getConfiguration().get('yamllint.path','yamllint');
  return new Promise((resolve) => {
    cp.spawn(yamllint_exe, ['--help'])
    .on('exit', resolve)
    .on('error', (error) => {
      vscode.window.showWarningMessage('yamllint cannot be launched: ' + error);
    });
  });
}

function lint(document: vscode.TextDocument) {
  if (document.languageId !== MODE.language) {
    return;
  }

  let yamllint_exe = vscode.workspace.getConfiguration().get('yamllint.path','yamllint');
  let yamllint_params = vscode.workspace.getConfiguration().get('yamllint.arguments', [''] );

  yamllint_params = yamllint_params.concat(["-f", "parsable", document.uri.fsPath]);

  let linter = cp.spawn(yamllint_exe , yamllint_params, { env: { 'LANG': 'C' } });

  linter.on('error', (error) => {
    vscode.window.showWarningMessage('yamllint cannot be launched: ' + error);
  });

  let reader = rl.createInterface(linter.stdout);
  let array: vscode.Diagnostic[] = [];

  reader.on('line', (line: string) => {
    // ^${document.uri.fsPath}:(\\d+?):\\d+?:\\s+\[(\\S+)\]\\s*(.*)\((.*)\)
    // ^${document.uri.fsPath}:(\\d+?):\\d+?:\\s*?(\[(?:warning|error)\])(\\S)+?:\\s*?(.+)
    let match = line.match(new RegExp(`^${document.uri.fsPath}:(\\d+?):\\d+?:\\s+\\[(\\S+)\\]\\s*(.*)\((.*)\)`));

    if (match !== null) {
      let diagnostic = new vscode.Diagnostic(
        document.lineAt(Number(match[1]) - 1).range, // Line number for the error
        match[3], // Human readable error message
        SEVERITY(match[2])
        // vscode.DiagnosticSeverity.Error // Severity
      );
      array.push(diagnostic);
    }
  });

  reader.on('close', () => {
    diagnostics.set(document.uri, array);
  });
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "yamllint" is now active!');

  checkSanity().then((exitCode) => {
    if (!exitCode && vscode.workspace.getConfiguration().get('yamllint.lint')) {
      diagnostics = vscode.languages.createDiagnosticCollection();

      vscode.workspace.onDidOpenTextDocument(lint);
      vscode.workspace.onDidSaveTextDocument(lint);
      vscode.workspace.textDocuments.forEach(lint);
      vscode.workspace.onDidCloseTextDocument((document) => {
        diagnostics.delete(document.uri);
      });

      context.subscriptions.push(diagnostics);
    }
  });
}

// this method is called when your extension is deactivated
export function deactivate() {
}
