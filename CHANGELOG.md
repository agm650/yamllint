# Change Log

## 0.1.0 - 29th March 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * First beta release

## 0.2.0 - 29th March 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Handling extra parameters for yamllint

## 0.2.1 - 29th March 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Solving an issue with the verification of the presence of the yamllint command
