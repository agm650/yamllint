# yamllint README

## Features

This plugins aims at helping people working with yaml files by displaying the output of yamllint.

![Linting yaml files](https://gitlab.com/agm650/yamllint/raw/master/images/yamllint.png "Linting YAML Files")

## Requirements

VSCode version greater or equals to 1.32.0

## Extension Settings

This extension contributes the following settings:

* `yamllint.lint`: enable/disable yaml linting
* `yamllint.path`: set to fullpath of the yamllint executable
* `yamllint.arguments`: Used to specify extra parameters to pass to yamllint, for instance patch to a configuration file.

![YAML configuration options](https://gitlab.com/agm650/yamllint/raw/master/images/yamllint_config.png "YAML configuration options")

## Known Issues

* Issues when manipulating non UTF-8 files

## Release Notes

### 0.1.0 - 29th March 2019

* First beta release of yamllint extension

### 0.2.0 - 29th March 2019

* Handling extra parameters for yamllint

### 0.2.1 - 29th March 2019

* Solving an issue with the verification of the presence of the yamllint command
